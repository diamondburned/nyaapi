package nyaapi

import "time"

// RFCDate contains the date
type RFCDate string

// Torrent has data for a torrent
type Torrent struct {
	ID           int64         `json:"id"`
	Name         string        `json:"name"`
	Status       int64         `json:"status"`
	Hash         string        `json:"hash"`
	Date         RFCDate       `json:"date"`
	Filesize     int64         `json:"filesize"`
	Description  string        `json:"description"`
	Comments     []interface{} `json:"comments"`
	SubCategory  int           `json:"sub_category,string"`
	Category     int           `json:"category,string"`
	Anidbid      int64         `json:"anidbid"`
	Vndbid       int64         `json:"vndbid"`
	Vgmdbid      int64         `json:"vgmdbid"`
	Dlsite       string        `json:"dlsite"`
	Videoquality string        `json:"videoquality"`
	Tags         interface{}   `json:"tags"`
	UploaderID   int64         `json:"uploader_id"`
	UploaderName string        `json:"uploader_name"`
	UploaderOld  string        `json:"uploader_old"`
	WebsiteLink  string        `json:"website_link"`
	Languages    []string      `json:"languages"`
	Magnet       string        `json:"magnet"`
	Torrent      string        `json:"torrent"`
	Seeders      int64         `json:"seeders"`
	Leechers     int64         `json:"leechers"`
	Completed    int64         `json:"completed"`
	LastScrape   RFCDate       `json:"last_scrape"`
	FileList     []interface{} `json:"file_list"`
}

// ParseDate parses the date
func (d RFCDate) ParseDate() (time.Time, error) {
	return time.Parse(string(d), time.RFC3339Nano)
}
