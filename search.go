package nyaapi

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

// Results are results from a search
type Results struct {
	Torrents         []Torrent `json:"torrents"`
	QueryRecordCount int64     `json:"queryRecordCount"`
	TotalRecordCount int64     `json:"totalRecordCount"`
}

// SearchArgs is the search arguments
type SearchArgs struct {
	Category    int
	SubCategory int
	Limit       int
}

// Search searches for torrents
func Search(q string, args *SearchArgs) (*Results, error) {
	vals := url.Values{
		"q": []string{q},
	}

	if args != nil {
		if args.Category != 0 && args.SubCategory != 0 {
			vals["c"] = []string{
				fmt.Sprintf(
					"%d_%d",
					args.Category, args.SubCategory,
				),
			}
		}

		if args.Limit != 0 {
			vals["limit"] = []string{strconv.Itoa(args.Limit)}
		}
	}

	resp, err := http.Get(Endpoint + "search?" + vals.Encode())

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	if err != nil {
		return nil, err
	}

	results := &Results{}

	if err := json.Unmarshal(body, results); err != nil {
		return nil, err
	}

	return results, nil
}
