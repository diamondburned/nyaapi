package nyaapi

import (
	"errors"
	"strings"
)

// NyaaCategoryMap contains all the categories in Nyaa.Pantsu
// https://github.com/NyaaPantsu/scrapers/blob/master/nyaa.go#L150
var NyaaCategoryMap = map[string][]int{
	"Art - Anime":                          []int{1, 1},
	"Art - Doujinshi":                      []int{1, 2},
	"Art - Games":                          []int{1, 3},
	"Art - Manga":                          []int{1, 4},
	"Art - Pictures":                       []int{1, 5},
	"Real Life - Photobooks and Pictures":  []int{2, 1},
	"Real Life - Videos":                   []int{2, 2},
	"Anime - Anime Music Video":            []int{3, 12},
	"Anime - English-translated":           []int{3, 5},
	"Anime - Non-English-translated":       []int{3, 13},
	"Anime - Raw":                          []int{3, 6},
	"Audio - Lossless":                     []int{2, 3},
	"Audio - Lossy":                        []int{2, 4},
	"Literature - English-translated":      []int{4, 7},
	"Literature - Non-English-translated":  []int{4, 14},
	"Literature - Raw":                     []int{4, 8},
	"Live Action - English-translated":     []int{5, 9},
	"Live Action - Idol/Promotional Video": []int{5, 10},
	"Live Action - Non-English-translated": []int{5, 18},
	"Live Action - Raw":                    []int{5, 11},
	"Pictures - Graphics":                  []int{6, 15},
	"Pictures - Photos":                    []int{6, 16},
	// "Software - Applications":              []int{1, 1},
	// "Software - Games":                     []int{1, 2},
}

// ParseCategory parses a category string
func ParseCategory(catstring string) (cat, subcat int, err error) {
	for catname, ints := range NyaaCategoryMap {
		if strings.Contains(
			strings.ToUpper(catname),
			strings.ToUpper(catstring),
		) {
			cat = ints[0]
			subcat = ints[1]
			return
		}
	}

	err = errors.New("Unknown category")

	return
}
